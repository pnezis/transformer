from setuptools import setup, find_packages

# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name="transformer",
    version='1.13.0',
    maintainer='VAIX.AI',
    maintainer_email='pn@vaix.ai',
    license="MIT-LICENSE",
    url='https://gitlab.com/pnezis/transformer',
    platforms=["any"],
    description="A transformer for python dictionaries, similar to tensorflow transform but much faster",
    long_description=long_description,
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    package_data={'': ['*.md', 'MIT-LICENSE']},
    include_package_data=True,
    install_requires=['numpy',
                      'scipy',
                      'scikit-learn',
                      'setuptools>=36.5.0',
                      'future',
                      'pyarrow'],
    setup_requires=['pytest-runner', 'tempdir'],
    tests_require=['pytest'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
    ]
)
