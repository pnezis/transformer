# Transform raw dictionaries

This package provide a simple class for transforming python dictionaries (raw
data) based on some configurable rules per column.

Like tensorflow transform but much faster and easier to use.


## Example usage

Assume a raw dictionary of the form:

```python
d = {
    'home': ['Panathinaikos', 'Levadiakos'],
    'away': ['Olympiakos', 'AEK'],
    'league': 'Superleague',
    'country': 'Greece',
    'odds': [3.22, 4.13]
}
```

In order to map the strings to ints we can use the class as following:

```python
from transformer.transformer import Transformer
from transformer.raw_metadata import RawMetadata, StringToIntHandler, MinMaxScalerHandler

# Set up the metadata
metadata = [
    RawMetadata('home', 'teams', StringToIntHandler()),
    # Notice that if we want to use the same handler again we must not
    # define it
    RawMetadata('away', 'teams'),
    RawMetadata('league', 'leagues', StringToIntHandler()),
    RawMetadata('country', 'countries', StringToIntHandler()),
    RawMetadata('odds', 'odds', MinMaxScalerHandler(minimum=0, maximum=1))
]

# initialize a transformer
t = Transformer(metadata=metadata)

# Process the dictionary
t.process(d)

# Finalize the transformer
t.finalize()

# Let's test it
t.transform(d)

# { 'away': [3, 4],
#    'country': 1,
#    'home': [1, 2],
#    'league': 1,
#    'odds': [0, 1]
# }

# let's test with something else
t.transform({'home': ['Olympiacos', 'Panathinaikos', 'PAOK']})
# {'home': [3, 1, 0]}

# Save the transformer
t.save('transformer')

# Load the transformer into an other instance and test it again
t2 = Transformer()
t2.load('transformer')
t2.transform({'away': ['AEK', 'Panathinaikos', 'Xanthi'], 'country': 'Greece'})
# {'away': [4, 1, 0], 'country': 1}
```

A specific column can also be processed directly by specifying the
`column_name` argument.

```python
t.process(['Panathinaikos', 'Levadiakos'], column_name='home')
t.transform(['Panathinaikos', 'Levadiakos', 'AEK'], column_name='home')
# Out: [1, 2, 0]
```

For more details check the API documentation

## Installation

Follow the instructions here
https://git.vaix.ai/vaix/devops/wikis/pypi

## Testing

To run the test you need to have the **pytest** module

```bash
pip install pytest
```

Then execute the following command on the root directory of the project 

```bash
pytest
```

## Upload to vaix pypi repository

In order to upload to the private vaix pypi repository you should issue the
following command once the package has been updated:

```bash
python setup.py sdist upload -r vaix
```
