from transformer.transformer import Transformer
from transformer.raw_metadata import (
    RawMetadata,
    StringToIntHandler,
    StandardScalerHandler,
    MinMaxScalerHandler,
    QuantileTransformerHandler)
import numpy as np
from tempdir import TempDir
import pytest
import os


d = {
    'first_name': ['String1', 'String2'],
    'last_name': ['String3', 'String4'],
    'numbers': [-1, 0, 1],
    'numbers2': [2, 4],
    'number': [2]
}


def test_string():
    string_metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(d)
    t_new = t.transform(d)
    assert t_new[0]['first_name'] == [1, 2]
    assert t_new[0]['last_name'] == ['String3', 'String4']


def test_none_string():
    string_metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(d)
    a = t.transform({'first_name': None})
    assert a[0]['first_name'] == 0


def test_transform_with_handler_name():
    string_metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(d['first_name'], column_name='first_name')
    t_new = t.transform(d['first_name'], column_name='first_name')
    assert t_new == [1, 2]


def test_string_int():
    test_dict = {'ids': ['abcd', '123', 45]}
    string_metadata = [
        RawMetadata('ids', 'ids', StringToIntHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(test_dict)
    t_new = t.transform(test_dict)
    assert t_new[0]['ids'] == [1, 2, 3]


def test_string_to_int_dictionary_size():
    """Should be the vocabulary size + '_oov_' symbol"""
    test_dict = {'ids': ['abcd', '123', 45]}
    string_metadata = [
        RawMetadata('ids', 'ids', StringToIntHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(test_dict)
    assert t.handlers['ids'].dictionary_size() == 4


def test_list_of_string():
    d_list = [{'name': ['str1', 'str2']},
              {'name': ['str3', 'str2']}]
    string_metadata = [
        RawMetadata('name', 'name', StringToIntHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(d_list)
    t_new = t.transform(d_list)
    assert t_new[0][0]['name'] == [1, 2]
    assert t_new[0][1]['name'] == [3, 2]


def test_nested_lists_of_strings():
    d_list = [{'name': ['str1', 'str2', ['str1', ['str2', ['str1', 'str3']]]]},
              {'name': ['str3', 'str2']}]
    string_metadata = [
        RawMetadata('name', 'name', StringToIntHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(d_list)
    t_new = t.transform(d_list)
    assert t_new[0][0]['name'] == [1, 2, [1, [2, [1, 3]]]]
    assert t_new[0][1]['name'] == [3, 2]


def test_two_strings_same_handler():
    string_metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler()),
        RawMetadata('last_name', 'name')
    ]
    t = Transformer(metadata=string_metadata)
    t.process(d)
    t_new = t.transform(d)
    assert t_new[0]['first_name'] == [1, 2]
    assert t_new[0]['last_name'] == [3, 4]


def test_string_to_int_set_all_tiems():
    string_metadata1 = [
        RawMetadata('values', 'values', StringToIntHandler())
    ]
    string_metadata2 = [
        RawMetadata('values', 'values', StringToIntHandler())
    ]

    t1 = Transformer(metadata=string_metadata1)
    t2 = Transformer(metadata=string_metadata2)

    d = {
        'values': ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    }

    t1.process(d)

    t2.handlers['values'].set_all_items(['a', 'b', 'c', 'a', 'b'])
    t2.handlers['values'].set_all_items(['b', 'd', 'c', 'a', 'e'])
    t2.handlers['values'].set_all_items(['d', 'd', 'f', 'a', 'g', 'a'])

    assert sorted(t1.handlers['values'].vocab.keys()) == sorted(t2.handlers['values'].vocab.keys())
    assert len(t1.handlers['values'].reverse_vocab) == len(t2.handlers['values'].reverse_vocab)


def test_transform_array():
    d = {'name': np.array([['a', 'b', 'c', 'd'],
                           ['a', 'e', 'f', 'g']]),
         'values1': np.array([1,2,3,4,5,6]),
         'values2': np.array([[1.,2,3,4],
                              [4,5,6,7],
                              [8,2,3,4]]),
         'values3': np.array([-10,32,45,21,2,3,4])}
    print(d)
    string_metadata = [
        RawMetadata('name', 'name', StringToIntHandler()),
        RawMetadata('values1', 'values1', MinMaxScalerHandler()),
        RawMetadata('values2', 'values2', StandardScalerHandler()),
        RawMetadata('values3', 'values3', QuantileTransformerHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(d)
    t.finalize()
    transformed1 = t.transform(d, single_pass=False)
    print(transformed1)
    transformed2 = t.transform(d, single_pass=True)
    assert (transformed1[0]['name'] == transformed2[0]['name']).all()
    assert (transformed1[0]['values1'] == transformed2[0]['values1']).all()
    assert (transformed1[0]['values2'] == transformed2[0]['values2']).all()
    assert (transformed1[0]['values3'] == transformed2[0]['values3']).all()

def test_numbers_min_max():
    numbers_metadata = [
        RawMetadata('numbers', 'numbers', MinMaxScalerHandler(0, 1))
    ]
    t = Transformer(metadata=numbers_metadata)
    t.process(d)
    t.finalize()
    t_new = t.transform(d)
    assert t_new[0]['numbers'] == [0., 0.5, 1.]


def test_numbers_min_max_no_finalization():
    numbers_metadata = [
        RawMetadata('numbers', 'numbers', MinMaxScalerHandler(0, 1))
    ]
    t = Transformer(metadata=numbers_metadata)
    t.process(d)
    with pytest.raises(ValueError):
        t.transform(d)


def test_numbers_min_max_equal_values():
    with pytest.raises(ValueError):
        numbers_metadata = [
            RawMetadata('numbers', 'numbers', MinMaxScalerHandler(0, 0))
        ]


def test_numbers_min_max_single_element():
    numbers_metadata = [
        RawMetadata('number', 'number', MinMaxScalerHandler(0, 1))
    ]
    t = Transformer(metadata=numbers_metadata)
    t.process(d)
    with pytest.raises(ValueError):
        t.finalize()


def test_numbers_scale():
    numbers_metadata = [
        RawMetadata('numbers2', 'numbers2', StandardScalerHandler())
    ]
    t = Transformer(metadata=numbers_metadata)
    t.process(d)
    t.finalize()
    t_new = t.transform(d)
    assert t_new[0]['numbers2'] == [-1, 1.]

def test_scaler_output_type():
    numbers_metadata = [
        RawMetadata('numbers2', 'numbers2', StandardScalerHandler())
    ]
    t = Transformer(metadata=numbers_metadata)
    t.process(d)
    t.finalize()
    t_new = t.transform(d)
    assert type(t_new[0]['numbers2'][0]) is float

def test_numbers_scale_no_finalization():
    numbers_metadata = [
        RawMetadata('numbers', 'numbers', StandardScalerHandler())
    ]
    t = Transformer(metadata=numbers_metadata)
    t.process(d)
    with pytest.raises(ValueError):
        t.transform(d)


def test_process_numpy_array():
    metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler()),
        RawMetadata('numbers', 'numbers', StandardScalerHandler())
    ]
    t = Transformer(metadata=metadata)
    t.process(np.array([d]))
    t.finalize()
    t.transform(np.array([d]))


def test_process_empty_list():
    metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler()),
        RawMetadata('numbers', 'numbers', StandardScalerHandler()),
        RawMetadata('numbers2', 'numbers2', MinMaxScalerHandler())
    ]
    t = Transformer(metadata=metadata)
    t.process([])


def test_process_empty_array():
    metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler()),
        RawMetadata('numbers', 'numbers', StandardScalerHandler()),
        RawMetadata('numbers2', 'numbers2', MinMaxScalerHandler())
    ]
    t = Transformer(metadata=metadata)
    t.process(np.array([]))


def test_transform_string_to_int_empty_list():
    metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler()),
    ]
    t = Transformer(metadata=metadata)
    t.process([])
    t.finalize()


def test_transform_standard_scaler_empty_list():
    metadata = [
        RawMetadata('numbers', 'numbers', StandardScalerHandler(filename='numbers.tsv')),
    ]
    t = Transformer(metadata=metadata)
    t.process([])
    with pytest.raises(ValueError):
        t.finalize()


def test_transform_standard_scaler_empty_list_after_finalize():
    metadata = [
        RawMetadata('numbers', 'numbers', StandardScalerHandler(filename='numbers.tsv')),
    ]
    t = Transformer(metadata=metadata)
    t.process({'numbers': [1, 2]})
    assert t.handlers['numbers'].value_list == [1, 2]
    t.finalize()
    assert not t.handlers['numbers'].value_list


def test_transform_min_max_scaler_empty_list():
    metadata = [
        RawMetadata('numbers2', 'numbers2', MinMaxScalerHandler())
    ]
    t = Transformer(metadata=metadata)
    t.process([])
    with pytest.raises(ValueError):
        t.finalize()


def test_save_tsvs():
    metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler(filename='my_special_name.tsv')),
        RawMetadata('numbers', 'numbers', StandardScalerHandler()),
        RawMetadata('numbers2', 'numbers2', MinMaxScalerHandler())
    ]
    t = Transformer(metadata=metadata)
    t.process(d)
    t.finalize()

    with TempDir() as temp:
        t.save_column_metafiles(temp)
        filename = os.path.join(temp, 'my_special_name.tsv')
        assert os.path.exists(filename)
        with open(filename, 'r') as embedding:
            lines = embedding.readlines()
            assert '_oov_\n' in lines[0]
            assert 'String1\n' in lines
            assert 'String2' in lines


def test_inverse_string():
    string_metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(d)
    assert t.inverse({'first_name': 1}) == {'first_name': 'String1'}


def test_inverse_string_list():
    string_metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(d)
    assert t.inverse({'first_name': [1, 2]}) == {'first_name':
                                                 ['String1', 'String2']}


def test_inverse_unknown_string():
    string_metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler())
    ]
    t = Transformer(metadata=string_metadata)
    t.process(d)
    assert t.inverse({'first_name': 13}) == {'first_name': '_oov_'}


def test_inverse_min_max_scaler():
    metadata = [
        RawMetadata('num', 'num', MinMaxScalerHandler())
    ]
    t = Transformer(metadata=metadata)
    tmp = {'num': [0, 1, 2]}
    t.process(tmp)
    t.finalize()
    assert t.inverse({'num': [0.0, 0.5, 1.0]}) == {'num': [0.0, 1.0, 2.0]}


def test_inverse_standard_scaler():
    metadata = [
        RawMetadata('num', 'num', StandardScalerHandler())
    ]
    t = Transformer(metadata=metadata)
    tmp = {'num': [1, 2, 3]}
    t.process(tmp)
    t.finalize()
    transformed = t.transform(tmp)[0]
    assert t.inverse(transformed) == tmp


def test_inverse_quantile_transformer():
    metadata = [
        RawMetadata('num', 'num', QuantileTransformerHandler())
    ]
    t = Transformer(metadata=metadata)
    tmp = {'num': [1, 2, 3]}
    t.process(tmp)
    t.finalize()
    transformed = t.transform(tmp)[0]
    inversed = t.inverse(transformed)
    epsilon = 0.000000001
    for i, value in enumerate(inversed['num']):
        assert abs(value-tmp['num'][i]) < epsilon

def test_inverse_quantile_out_of_scale():
    metadata = [
        RawMetadata('num', 'num', QuantileTransformerHandler())
    ]
    t = Transformer(metadata=metadata)
    tmp = {'num': [-100, 0, 100]}
    t.process(tmp)
    t.finalize()
    h = t.handlers['num']
    assert h.transform_single_item(100) == h.transform_single_item(200)
    assert h.transform_single_item(-100) == h.transform_single_item(-200)

    v = h.transform_single_item(-500)
    assert h.inverse_single_item(v) == -100

    v = h.transform_single_item(500)
    assert h.inverse_single_item(v) == 100


def test_save_load_transformer():
    data = {
        'first_name': ['String1', 'String2'],
        'last_name': ['String3', 'String4'],
        'numbers': [-1, 0, 1],
        'numbers2': [2, 4],
    }
    metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler()),
        RawMetadata('numbers2', 'numbers2', StandardScalerHandler()),
        RawMetadata('numbers', 'numbers', MinMaxScalerHandler())
    ]
    t = Transformer(metadata=metadata)
    t.process(data)
    t.finalize()

    with TempDir() as temp:
        filename = os.path.join(temp, 'tmp_transformer')
        t.save(filename)
        assert os.path.exists(filename)
        t2 = Transformer()
        t2.load(filename)
        t_new = t2.transform(data)
        assert t_new[0]['first_name'] == [1, 2]
        assert t_new[0]['numbers'] == [0., 0.5, 1.]
        assert t_new[0]['numbers2'] == [-1., 1.]


def test_save_load_transformer_from_file_object():
    data = {
        'first_name': ['String1', 'String2'],
        'last_name': ['String3', 'String4'],
        'numbers': [-1, 0, 1],
        'numbers2': [2, 4],
    }
    metadata = [
        RawMetadata('first_name', 'name', StringToIntHandler()),
        RawMetadata('numbers2', 'numbers2', StandardScalerHandler()),
        RawMetadata('numbers', 'numbers', MinMaxScalerHandler())
    ]
    t = Transformer(metadata=metadata)
    t.process(data)
    t.finalize()

    with TempDir() as temp:
        filename = os.path.join(temp, 'tmp_transformer')
        with open(filename, 'wb') as f:
            t.save(f)
        assert os.path.exists(filename)
        t2 = Transformer()
        with open(filename, 'rb') as f:
            t2.load(f)
        t_new = t2.transform(data)
        assert t_new[0]['first_name'] == [1, 2]
        assert t_new[0]['numbers'] == [0., 0.5, 1.]
        assert t_new[0]['numbers2'] == [-1., 1.]


def test_load_with_finalize_issue():
    data = {
        'numbers': [-1, 0, 1],
        'numbers2': [2, 4],
    }
    metadata = [
        RawMetadata('numbers', 'numbers', StandardScalerHandler()),
        RawMetadata('numbers2', 'numbers2', MinMaxScalerHandler())
    ]
    t = Transformer(metadata=metadata)
    t.process(data)
    t.finalize()

    with TempDir() as temp:
        filename = os.path.join(temp, 'tmp_transformer')
        t.save(filename)
        t2 = Transformer()
        t2.load(filename)
        t2.finalize()


def test_min_max_scaler_handler_with_provided_values():
    data = {
        'numbers': [-1, 0, 1],
    }
    metadata = [
        RawMetadata('numbers', 'numbers', MinMaxScalerHandler(data_minimum=-2, data_maximum=2))
    ]
    t = Transformer(metadata=metadata)
    t_new = t.transform(data)
    assert t_new[0]['numbers'] == [0.25, 0.5, 0.75]


def test_standard_scaler_handler_with_provided_values():
    data = {
        'numbers': [2, 4],
    }
    metadata = [
        RawMetadata('numbers', 'numbers', StandardScalerHandler(data_mean=1., data_std=2.))
    ]
    t = Transformer(metadata=metadata)
    t_new = t.transform(data)
    assert t_new[0]['numbers'] == [0.5, 1.5]
