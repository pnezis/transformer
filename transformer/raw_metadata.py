from abc import ABCMeta
from abc import abstractmethod
from collections import OrderedDict
import math
from sklearn import preprocessing
import numpy as np
import sys


class ColumnHandler:
    """Abstract class for handling column transforms

    All column handlers must inherit from this class. It provides functionality
    for processing and processing raw column data.

    Args:
        filename (string): The filename used in the save function (default '')
        accept_lists (bool): if set all list items are handled one by one,
            otherwise a ValueError is raised (default True)
        process_all_list_items (bool): if set all list items will be processed,
            otherwise only the last one will be used. Should be set when the
            raw data are created sequentially and the previous list elements
            have already been provided (default True)
        transform_all_list_items (bool): if set all list items will be
            transformed, otherwise only the last one will be processed
            (default True)
    """
    __metaclass__ = ABCMeta

    def __init__(self,
                 filename='',
                 accept_lists=True,
                 process_all_list_items=True,
                 transform_all_list_items=True):
        """Column Handler init function

        Keyword arguments:
        """
        self.filename = filename
        self.accept_lists = accept_lists
        self.process_all_list_items = process_all_list_items
        self.transform_all_list_items = transform_all_list_items
        self.can_transform = False

    def inverse(self, item):
        """Inverse transformation of an item. The item can even be a list or a
        single object

        The subclasses should validate that the object type is the expected one
        Every time this function is called then the can_transform flag is set
        to False
        """
        if self.needs_finalization() and not self.can_transform:
            raise ValueError('Cannot inverse column without calling '
                             'finalize first')
        return self.__handle_item(item,
                                  self.inverse_single_item,
                                  self.process_all_list_items)

    def process(self, item):
        """Processes an item. The item can even be a list or a single object

        The subclasses should validate that the object type is the expected one
        Every time this function is called then the can_transform flag is set
        to False
        """
        self.__handle_item(item,
                           self.process_single_item,
                           self.process_all_list_items)
        self.can_transform = False

    def transform(self, item, single_pass=False):
        """Transforms the given item. The item can even be a list or a single
        object.

        If single_pass is set to True then the fast transformation method is
        used. In this case the item must be an ndarray.

        Notice that finalize should have been called first if it is required
        otherwise a ValueError will be raised.
        """
        if self.needs_finalization() and not self.can_transform:
            raise ValueError('Cannot transform column without calling '
                             'finalize first')

        if single_pass:
            # We have to reshape the input to a one dimensional array in order
            # to apply the transformations properly
            if not isinstance(item, np.ndarray):
                raise ValueError('Cannot transform a not ndarray if the '
                                 'option single_pass is set')
            shape = item.shape
            values = item
            while len(values.shape) != 1:
                values = np.concatenate(values)
            t = self.transform_array(values)
            return t.reshape(shape)
        else:
            return self.__handle_item(item,
                                      self.transform_single_item,
                                      self.transform_all_list_items)

    def __handle_item(self, item, function, all_items_flag):
        if not isinstance(item, list) and not isinstance(item, np.ndarray):
            return function(item)

        # It is a list so handle it based on the options
        if not self.accept_lists:
            raise ValueError('Handler does not support lists')
        if not all_items_flag:
            return function(item[-1])
        return [self.__handle_item(i, function, all_items_flag) for i in item]

    def needs_finalization(self):
        """Returns True if the column handler requires the finalize function to
        be called before applying a transform. This should be necessary if the
        handler needs to aggregate values from all entries before being
        applied correctly (e.g. ScaleColumn).

        In order to be on the safe side it returns True by default. Child
        classes should explicitely override it if the finalization is not
        required
        """
        return True

    @abstractmethod
    def finalize(self):
        """Finalize the transform internals

        This should be implemented by all subclasses even if they do not need
        it.
        """
        raise NotImplementedError()

    @abstractmethod
    def inverse_single_item(self, item):
        """Process a single item
        """
        raise NotImplementedError()

    @abstractmethod
    def process_single_item(self, item):
        """Process a single item
        """
        raise NotImplementedError()

    @abstractmethod
    def transform_single_item(self, item):
        """Transform a single item
        """
        raise NotImplementedError()

    @abstractmethod
    def set_all_items(self, items):
        """Mass insertion of all handler items. A one dimensional numpy array
        is expected
        """
        raise NotImplementedError()

    @abstractmethod
    def transform_array(self, items):
        """Mass transformation of the complete array. Instead of looping
        through each element and transforming each item separately a complete
        array is transformed at once using scikit and optimized functions
        """
        raise NotImplementedError()

    def save(self, filename):
        """Save to a file the handler's data, eg the dictionary in a tsv
        format. The default implementation is empty
        """
        pass

    def export(self):
        """Export the current handler configuration as json. The default
        implementation returns an empty dictionary
        """
        return {}


class DefaultHandler(ColumnHandler):
    """The default column handler which does not apply any transformation to
    the provided inputs.
    """
    def __init__(self, **kwargs):
        super(DefaultHandler, self).__init__(**kwargs)

    def process_single_item(self, item):
        pass

    def finalize(self):
        pass

    def transform_single_item(self, item):
        return item

    def needs_finalization(self):
        return False


class StringToIntHandler(ColumnHandler):
    """Map strings to ints

    All processed words are kept in an ordered dictionary.
    The default value returned if a word is not present
    in the dictionary is 0, and the starting index of valid
    words is 1. This way no finalization is required for
    applying a transformation.

    Args:
        ignore (list): List of words that should be ignored and not added in
            the dictionary (default [])

    Raises:
        ValueError if ignore contains non strings
    """
    def __init__(self, ignore=[], **kwargs):
        super(StringToIntHandler, self).__init__(**kwargs)
        for item in ignore:
            if not isinstance(item, str):
                raise ValueError('ignore argument of StringToIntHandler '
                                 'can contain only strings')
        self.ignore = ignore
        self.vocab = OrderedDict()
        self.reverse_vocab = OrderedDict()
        self.vocab['_oov_'] = 0  # Add the 'out of vocabulary' symbol
        self.reverse_vocab[0] = '_oov_'
        self.default = 0

    def set_all_items(self, items):
        unique_items = np.unique(items)
        new_items = set(unique_items).difference(self.vocab.keys())
        start = len(self.vocab)
        vocab_upd = {key: start+index for index, key in enumerate(new_items)}
        reverese_upd = {start+index: key for index, key in enumerate(new_items)}
        self.vocab.update(vocab_upd)
        self.reverse_vocab.update(reverese_upd)

    def process_single_item(self, item):
        """Add the given item to the dictionary

        If the item is already in the dictionary then nothing happens,
        otherwise is is added with index value start+len(dict).

        Args:
            item (string): The item to be processed

        Raises:
            ValueError if item is not string
        """
        if sys.version_info < (3, ):
            if isinstance(item, unicode):
                item = item.encode('utf-8')
        else:
            if isinstance(item, bytes):
                item = item.decode('utf-8')

        if isinstance(item, int):
            item = str(item)

        if not isinstance(item, str):
            raise ValueError('Handler [{}]: Failed to process [{}:{}]. '
                             'StringToIntHandler can only handle '
                             'strings'.format(self.filename,
                                              item,
                                              str(type(item))))

        if item not in self.vocab and item not in self.ignore:
            v = len(self.vocab)
            self.vocab[item] = v
            self.reverse_vocab[v] = item

    def needs_finalization(self):
        return False

    def finalize(self):
        pass

    def transform_single_item(self, item):
        """Transform the given item

        If the item is in the dictionary then its index is returned, otherwise
        it returns the default value

        Args:
            item (string): The item to be processed

        Raises:
            ValueError if item is not string
        """
        if sys.version_info < (3, ):
            if isinstance(item, unicode):
                item = item.encode('utf8')
        else:
            if isinstance(item, bytes):
                item = item.decode('utf8')

        if isinstance(item, int):
            item = str(item)

        if item is None:
            return self.default

        if not isinstance(item, str):
            raise ValueError('Handler [{}]: Failed to transform [{}:{}]. '
                             'StringToIntHandler can only handle '
                             'strings'.format(self.filename,
                                              item,
                                              str(type(item))))

        return self.vocab.get(item, self.default)

    def save(self, filename):
        """Saves the dictionary in tsv format
        """
        with open(filename, 'w') as outfile:
            outfile.write('\n'.join(self.vocab.keys()))

    def dictionary_size(self):
        return len(self.vocab)

    def inverse_single_item(self, item):
        # print 'called inverse_single_item {}'.format(item)
        if not isinstance(item, int):
            raise ValueError('Handler [{}]: Failed to inverse [{}:{}]. '
                             'StringToIntHandler can only inverse '
                             'integers'.format(self.filename,
                                               item,
                                               str(type(item))))
        return self.reverse_vocab.get(item, '_oov_')

    def export(self):
        d = {
            'type': 'string2int',
            'default': self.default,
            'mappings': {}
        }
        for key, value in self.vocab.items():
            d['mappings'][key] = value
        return d

    def transform_array(self, items):
        return np.array([self.vocab.get(i, 0) for i in items])


class MinMaxScalerHandler(ColumnHandler):
    """Scale integers/floats between a minimum and a maximum value.

    Assuming x_min and x_max are the minimum and maximum values of the dataset,
    while y_min, y_max the transformed minimum and maximum values then the
    transformation is y = f(x) = min_y + (x-min_x)*(max_y-min_y)/(max_x-min_x)

    Note that if both values for data_minimum and data_maximum are provided
    the handler will be finalized during the initialization. Otherwise,
    these values are ignored

    Args:
        minimum: The minimum value after the transformation. Default:0
        maximum: The maximum value after the transformation. Default:1
        data_minimum: The minimum value encountered on the original data. Default: None
        data_maximum: The maximum value encountered on the original data. Default: None

    Raises:
        ValueError if minimum, maximum cannot be transformed into numbers
    """
    def __init__(self, minimum=0, maximum=1,
                 data_minimum=None, data_maximum=None,
                 **kwargs):
        if minimum >= maximum:
            raise ValueError('User defined minimum value is greater or equal to maximum value')
        super(MinMaxScalerHandler, self).__init__(**kwargs)
        self.min_y = float(minimum)
        self.max_y = float(maximum)
        if data_maximum is not None and data_maximum is not None:
        # Both min/max values on the original data are provided. Finalize the handler
            self.min_x = float(data_minimum)
            self.max_x = float(data_maximum)
            self.finalize()
        else:
            self.min_x = float('inf')  # Initialize to the largest value
            self.max_x = float('-inf')  # Initialize to the smallest value possible
            self.mult = 1  # Once finalized we compute the quantity (max_y-min_y)/(max_x-min_x) once

    def set_all_items(self, items):
        self.min_x = np.amin(items)
        self.max_x = np.amax(items)
        self.finalize()

    def process_single_item(self, item):
        """Adjust the x range.

        If the item is smaller than min_x or larger than max_x then
        adjust the range. Process takes place only if
        the handler is not finalized yet.

        Args:
            item (float): The item to be processed

        Raises:
            ValueError if item cannot be cast into a float
        """
        if not self.can_transform:
            self.min_x = min(self.min_x, float(item))
            self.max_x = max(self.max_x, float(item))

    def transform_single_item(self, item):
        """Transform the given item

        Returns the new value

        Args:
            item (float): The item to be processed

        Raises:
            ValueError if item cannot be cast into a float
        """
        return self.min_y + (float(item) - self.min_x) * self.mult

    def finalize(self):
        """Create the multiplier for the data transform.
        y = f(x) = min_y + (x-min_x)*(max_y-min_y)/(max_x-min_x)=
        min_y + (x-min_x)*mult
        Finalize is performed only once.

        Raises:
            ValueError if list is empty, there is only 1 element or
            all elements have the same values
        """
        if not self.can_transform:
            if self.min_x >= self.max_x:
                raise ValueError('Minimum element on dataset is greater or equal to maximum element. '
                                 'Possibly the dataset contains less than two elements or '
                                 'all elements have equal value')
            self.mult = (self.max_y - self.min_y) / (self.max_x - self.min_x)
            self.can_transform = True

    def inverse_single_item(self, item):
        """Process a single item
        """
        return self.min_x + (float(item)-self.min_y)/self.mult

    def transform_array(self, items):
        return self.min_y + (items - self.min_x) * self.mult


class StandardScalerHandler(ColumnHandler):
    """Scale integers/floats between a user-defined mean value and standard deviation
    using scikit-learn.preprocessing.StandardScaler

    Note that if both values for data_mean and data_std are provided
    the handler will be finalized during the initialization. Otherwise,
    these values are ignored

    Args:
        data_mean: The average value of the original data. Default: None
        data_std: The standard deviation of the original data. Default: None

    Raises:
        ValueError if minimum, maximum cannot be transformed into numbers
    """
    def __init__(self, minimum=0, maximum=1,
                 data_mean=None, data_std=None, **kwargs):
        super(StandardScalerHandler, self).__init__(**kwargs)
        self.value_list = []  # Will store all the numbers here
        self.scaler = preprocessing.StandardScaler()
        if data_mean is not None and data_std is not None:
            self.value_list = [-1., 1.]  # Random input to avoid ValueError when fitting an empty list.
            self.finalize()
            self.scaler.mean_[0] = float(data_mean)
            self.scaler.scale_[0] = float(data_std)
            self.scaler.var_[0] = math.pow(float(data_std), 2.)
            pass

    def set_all_items(self, items):
        if not isinstance(items, np.ndarray):
            items = np.array(items)
        self.scaler.fit(items.reshape(-1, 1))
        self.can_transform = True

    def process_single_item(self, item):
        """Store the number internally.
        Necessary to compute the mean and std.
        Values are only stored if the handler is not finalized.

        Args:
            item (float): The item to be processed

        Raises:
            ValueError if item cannot be cast into a float
        """
        if not self.can_transform:
            self.value_list.append(float(item))

    def transform_single_item(self, item):
        """Transform the given item

        Returns the new value

        Args:
            item (float): The item to be processed

        Raises:
            ValueError if item cannot be cast into a float
        """
        # Need to convert to a Numpy array and reshape it to 2D array to
        # avoid the DeprecationWarning
        return float(self.scaler.transform(np.array([float(item)]).reshape(1, -1))[0, 0])

    def finalize(self):
        """Create the data transform.
        Finalize is performed only once.

        Raises:
            ValueError if list is empty
        """
        # Need to convert to a Numpy array and reshape it to 2D array to
        # avoid the DeprecationWarning
        if not self.can_transform:
            self.scaler.fit(np.array(self.value_list).reshape(-1, 1))
            self.value_list = []
            self.can_transform = True

    def inverse_single_item(self, item):
        """Process a single item
        """
        return self.scaler.inverse_transform(np.array([float(item)]).reshape(1,-1))[0, 0]


    def transform_array(self, items):
        return self.scaler.transform(items.reshape(-1, 1))[:, 0]


class QuantileTransformerHandler(ColumnHandler):
    """Transform numbers into the same distribution. The cumulative density
    function of a feature is used to project the original values. Features
    values of new/unseen data that fall below or above the fitted range will be
    mapped to the bounds of the output distribution. Note that this transform
    is non-linear. It may distort linear correlations between variables
    measured at the same scale but renders variables measured at different
    scales more directly comparable.

    The QuantileTransformer of scikit learn is used.

    Args:
        n_quantiles: The number of quantiles to be computed. It corresponds to
            the number of landmarks used to discetize the cumulative density
            function. Default: 1000
        distribution: Marginal distribution for the transformed data. The
            supported options are 'uniform' (default) and 'normal'
    """
    def __init__(self, n_quantiles=1000, distribution='uniform', **kwargs):
        super(QuantileTransformerHandler, self).__init__(**kwargs)
        self.value_list = []
        self.scaler = preprocessing.QuantileTransformer(n_quantiles=n_quantiles,
                                                        output_distribution=distribution)

    def set_all_items(self, items):
        if not isinstance(items, np.ndarray):
            items = np.array(items)
        self.scaler.fit(items.reshape(-1, 1))
        self.can_transform = True

    def process_single_item(self, item):
        """Store the number internally.
        Values are only stored if the handler is not finalized.

        Args:
            item (float): The item to be processed

        Raises:
            ValueError if item cannot be cast into a float
        """
        if not self.can_transform:
            self.value_list.append(float(item))

    def transform_single_item(self, item):
        """Transform the given item

        Returns the new value

        Args:
            item (float): The item to be processed

        Raises:
            ValueError if item cannot be cast into a float
        """
        # Need to convert to a Numpy array and reshape it to 2D array to
        # avoid the DeprecationWarning
        return float(self.scaler.transform(np.array([float(item)]).reshape(1, -1))[0, 0])

    def finalize(self):
        """Create the data transform.
        Finalize is performed only once.

        Raises:
            ValueError if list is empty
        """
        # Need to convert to a Numpy array and reshape it to 2D array to
        # avoid the DeprecationWarning
        if not self.can_transform:
            self.scaler.fit(np.array(self.value_list).reshape(-1, 1))
            self.value_list = []
            self.can_transform = True

    def inverse_single_item(self, item):
        """Process a single item
        """
        return self.scaler.inverse_transform(np.array([float(item)]).reshape(1,-1))[0, 0]

    def transform_array(self, items):
        return self.scaler.transform(items.reshape(-1, 1))[:, 0]

class RawMetadata:
    """Transform metadata for a raw column

    Args:
        raw_name: The name of the key in the raw data dictionary
        handler_name: The name of the handler that will be used
        handler: The ColumnHandler that will be used for this specific column.
            If None then this column will not be processed
        process: If set the column will be used during processing, otherwise
            only during transformation
    """

    def __init__(self, raw_name, handler_name, handler=None, process=True):
        self.raw_name = raw_name
        self.handler_name = handler_name
        self.handler = handler
        self.process = process

