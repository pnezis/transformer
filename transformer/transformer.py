try:
    import cPickle as pickle
except:
    import pickle
from future.utils import iteritems
import numpy as np
import os
import json
from .raw_metadata import RawMetadata, ColumnHandler
from collections import defaultdict
import pyarrow as pa
import pyarrow.parquet as pq


def is_file_like(obj):
    """
    Check if the object is a file-like object.

    For objects to be considered file-like, they must be an iterator AND have
    either a `read` and/or `write` method as an attribute.
    Note: file-like objects must be iterable, but iterable objects need not be
    file-like.
    """

    if not (hasattr(obj, "read") or hasattr(obj, "write")):
        return False

    if not hasattr(obj, "__iter__"):
        return False

    return True


class Transformer(object):
    """Transform an input dictionary given the specified metadata

    The metadata describe the mapping between the raw dcitionary keys to the
    transformation handlers that will be used. If the same handler name is
    used in more than one raw columns then the first handler will be used.

    Args:
        metadata: A list with metadata for raw columns
        dismiss_missing_columns (bool): If set then the columns that are not
            present in the metadata will be dismissed during transformation

    Raises:
        ValueError if the metadata list does not contain instances of
            RawMetadata
        ValueError if the same column name is provided more than once
        ValueError if a handler is provided for an already assigned handler
            name
        ValueError if the provided handler for a column is not an instance of
            ColumnHandler
    """
    def __init__(self, metadata=[], dismiss_missing_columns=False):
        self.handlers = {}
        self.metadata = {}
        self.dismiss_missing_columns = dismiss_missing_columns
        for m in metadata:
            self.add_handler_metadata(m)

    def handler_columns(self, handler, process=None):
        columns = []
        for column, metadata in self.metadata.items():
            if metadata.handler_name == handler:
                if process is None or metadata.process == process:
                    columns.append(column)
        return columns

    def add_handler_metadata(self, m):
        if not isinstance(m, RawMetadata):
            raise ValueError('Metadata values must be instances of '
                             'RawMetadata')
        if m.raw_name in self.metadata:
            raise ValueError('You have already provided a definition '
                              'for column {}'.format(m.raw_name))
        self.metadata[m.raw_name] = m
        # add a new handler if it does not exist already
        if m.handler_name in self.handlers:
            if m.handler is not None:
                raise ValueError('You have already defined a handler with'
                                 ' handler name {}'.format(m.handler_name))
            return

        if not isinstance(m.handler, ColumnHandler):
            raise ValueError('Metadata handler must be an instance of '
                             'ColumnHandler')

        self.handlers[m.handler_name] = m.handler

    def inverse(self, raw_data):
        """Inverse transformation of given raw data

        """
        if isinstance(raw_data, list) or \
           isinstance(raw_data, np.ndarray):
            return [self.inverse(item) for item in raw_data]

        inversed = {}
        for k in raw_data.keys():
            if k in self.metadata:
                h = self.metadata[k].handler_name
                inversed[k] = self.handlers[h].inverse(raw_data[k])
            else:
                if not self.dismiss_missing_columns:
                    inversed[k] = raw_data[k]
        return inversed

    def process(self, raw_data, column_name=None):
        """Process the given raw_data

        If a list of dictionaries is provided then all of them are processed by
        default. For every key in the dictionary the corresponding handler (if
        configured) is used in order to process the item value.

        If a column_name is set then directly the associated handler is used
        for transforming the given data (which are expected to be a list or a
        single item)

        Args:
            raw_data : Dictionary or list of dictionaries
            column_name : The column to be used. Default is None

        Raises:
            ValueError if there is no accosicated handler for the provided
                column
        """
        if column_name is not None:
            if column_name not in self.metadata:
                raise ValueError('No accociated handler found for column "{}"'.
                                 format(column_name))
            return self.process({column_name: raw_data})

        if isinstance(raw_data, list) or \
           isinstance(raw_data, np.ndarray):
            for item in raw_data:
                self.process(item)
        else:
            for k in raw_data.keys():
                if k not in self.metadata:
                    continue
                if not self.metadata[k].process:
                    continue
                self.handlers[self.metadata[k].handler_name].process(raw_data[k])

    def set_values(self, name, values):
        """Sets a set of values to a column handler

        Args:
            name: The handler that will be used
            values: A list with the values that will be set

        Raises:
            ValueError if values is not a list
            ValueError if name is an invalid handler
        """
        if name not in self.handlers:
            raise ValueError('Invalid handler name {}'.format(name))
        if not isinstance(values, list) and\
           not isinstance(values, np.ndarray):
            raise ValueError('values must be a list or an np array')

        self.handlers[name].process(values)

    def transform(self,
                  raw_data,
                  column_name=None,
                  single_pass=False,
                  verbose=False):
        """Transform the given raw_data

        Every value of the dictionary is transformed based on the configured
        handler. If the flag dismiss_missing_columns is set to the transformer
        and a key has not a configured handler then the column is not copied
        to the transformed dictionary, otherwise we use the input value
        without transforming it.

        If a list of dictionaries is provided then a list of transformed
        dictionaries is returned.

        If column_name is not None then the associated handler will be used
        for transforming the provided data.

        Note:
            You should call finalize first if this is required by at least one
            of the configured column handlers.

        Args:
            raw_data : Dictionary or list of dictionaries
            column_name : If set then the associated handler will be used for
                transforming the data
                single_pass: If set then all values will be transformed at once
                instead of looping through all elements.
            single_pass : If set the vectorized transformation is applied to
                the provided columns. Works only with np arrays
            verbose : If set debug messages are printed

        Raises:
            ValueError if there is no associated handler for the provided
                column
        """
        if column_name is not None:
            if column_name not in self.metadata:
                raise ValueError('No accociated handler found for column "{}"'.
                                 format(column_name))
            return self.transform({column_name: raw_data})[0][column_name]

        if isinstance(raw_data, list) or \
           isinstance(raw_data, np.ndarray):
            result = [self.transform(item,
                                     single_pass=single_pass) for item in raw_data]
            return [i[0] for i in result], [i[1] for i in result]

        transformed = {}
        features = {}
        for k in raw_data.keys():
            if k in self.metadata:
                if verbose:
                    print('transforming column {}'.format(k))
                h = self.metadata[k].handler_name
                transformed[k] = self.handlers[h].transform(raw_data[k],
                                                            single_pass=single_pass)
            else:
                if not self.dismiss_missing_columns:
                    transformed[k] = raw_data[k]

        return transformed, None

    def init_from_parquet(self, path):
        """Initializes the transformer from a set of parquet files. Every
        subfolder in the given path corresponds to a data column. The data
        are loaded per column and set to the handler at once.

        A saved transformer object is expected at the root of the given path.
        If it does not exist an exception will be raised. Transformer values
        are expected to be found in a folder called transformer_values
        """
        transformer_path = os.path.join(path, 'transformer')
        if not os.path.exists(transformer_path):
            raise ValueError('No transformer found in the expected path: {}'
                             ''.format(transformer_path))
        print('loading transformer from {}'.format(transformer_path))
        self.load(transformer_path)

        columns_path = os.path.join(path, 'transformer_values')
        if not os.path.exists(columns_path):
            raise ValueError('No columns folder found in expected pathL {}'
                             ''.format(columns_path))

        handler_columns = defaultdict(list)
        for m in self.metadata.values():
            handler_columns[m.handler_name].append(m.raw_name)

        for handler, columns in handler_columns.items():
            items = np.array([])
            for c in columns:
                c_path = os.path.join(columns_path, c)
                if not os.path.exists(c_path):
                    continue
                print('loading handler "{}" values from: {}'.format(handler,
                                                                    c_path))
                c_values = pq.read_pandas(c_path).to_pandas()[c].values
                items = np.concatenate((items, c_values))
            if len(items) > 0:
                print('initializing handler "{}" with {} '
                      'values'.format(handler, len(items)))
                self.handlers[handler].set_all_items(items)

    def finalize(self):
        """Finalize the column handlers

        This function must be called if at lest one of the configured handlers
        requires finalization.
        """
        # for key, handler in self.handlers.iteritems():
        for key, handler in iteritems(self.handlers):
            handler.finalize()

    def transform_pandas_dataframe(self, df):
        for key in df.keys():
            if key not in self.metadata:
                continue
            h = self.metadata[key].handler_name
            df[key] = self.handlers[h].transform(df[key].values)

        return df

    def load(self, filename_or_file):
        """Load a transformer class from a saved instance

        Args:
            filename_or_file : path of the saved transformer instance or an
            opened file object
        """
        tmp_dict = {}
        if not is_file_like(filename_or_file):
            with open(filename_or_file, 'rb') as f:
                tmp_dict = pickle.load(f)
        else:
            tmp_dict = pickle.load(filename_or_file)

        self.__dict__.update(tmp_dict)


    def save(self, filename_or_file):
        """Save the transformer to a file

        Args:
            filename_or_file : path of the file where the transformer will be
            saved or a file object
        """
        if not is_file_like(filename_or_file):
            print('saving to {}'.format(filename_or_file))
            with open(filename_or_file, 'wb') as f:
                pickle.dump(self.__dict__, f, 2)
        else:
            pickle.dump(self.__dict__, filename_or_file, 2)

    def export(self, filename):
        """Export the transformer in json format

        Args:
            filename: path of the file where the exported json will be saved
        """
        d = {}
        d['metadata'] = {}
        for column, metadata in self.metadata.items():
            d['metadata'][column] = metadata.handler_name
        d['handlers'] = {}
        for name, handler in self.handlers.items():
            d['handlers'][name] = handler.export()

        with open(filename, 'w') as outfile:
            json.dump(d, outfile, sort_keys=True, indent=4)

    def save_column_metafiles(self, output_path):
        """Save handler metafiles

        The metafiles of all handlers that support this functionality will be
        saved at the given path. The default filename is of the form
        'raw_column_name.tsv'

        Args:
            output_path : The paths where the metafiles will be saved
        """
        for handler_name, handler in iteritems(self.handlers):
            filename = handler.filename
            if not filename:
                filename = handler_name
            filename = os.path.join(output_path, filename)
            handler.save(filename)

    def needs_finalization(self):
        """Check if the transormer can be used directly or finalization is
        required first
        """
        for key, handler in iteritems(self.handlers):
            if handler.needs_finalization():
                return True
        return False

    def handler(self, name):
        if name not in self.handlers:
            raise ValueError('No handler found with name {}'.format(name))
        return self.handlers[name]
